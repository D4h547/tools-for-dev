# Configuración de Commitlint y Husky para Proyectos de Software

Este script automatiza la configuración de `commitlint` y `Husky` en proyectos de software, asegurando que todos los mensajes de commit sigan una convención estandarizada. Establecer una convención de mensajes de commit ayuda a mantener un historial de cambios claro y coherente, lo que facilita la colaboración en equipo y mejora la gestión del proyecto.

## Descripción

El script `setup-commitlint-husky.sh` realiza los siguientes pasos automáticamente:

1. Verifica si Node.js y npm están instalados en el sistema.
2. Inicializa un proyecto npm si no existe un `package.json`.
3. Instala las dependencias necesarias para `commitlint` y `Husky`.
4. Crea un archivo de configuración para `commitlint`.
5. Configura `Husky` para utilizar `commitlint` al hacer commits, garantizando que todos los mensajes de commit cumplan con la convención establecida.
6. Hace el script de `Husky` ejecutable para asegurar que el hook de `commit-msg` funcione correctamente.

## Por Qué Es Una Buena Práctica

Adoptar `commitlint` y `Husky` en tu flujo de trabajo de Git ofrece varias ventajas:

- **Consistencia en Mensajes de Commit**: Facilita la lectura y comprensión del historial de cambios.
- **Mejora la Colaboración**: Ayuda a los equipos a seguir una guía común, lo que reduce los errores y mejora la eficiencia.
- **Automatización de Procesos**: Permite integrar otras herramientas de CI/CD que pueden utilizar los mensajes de commit para versionado automático, generación de notas de lanzamiento, etc.
- **Facilita el Mantenimiento y la Revisión de Código**: Un historial de commit claro ayuda a identificar y revertir cambios problemáticos más rápidamente.

## Universalidad de la Configuración

La configuración de `commitlint` y `Husky` es independiente del lenguaje de programación utilizado en el proyecto. Dado que estas herramientas operan a nivel del sistema de control de versiones Git, pueden ser implementadas en cualquier proyecto que utilice Git, sin importar si trabajas con Python, JavaScript, Ruby, Java, C#, o cualquier otro lenguaje de programación.

### Ventajas en Diversos Proyectos

- **Automatización**: Facilita la integración con herramientas de CI/CD, permitiendo automatizar el versionado, la generación de notas de lanzamiento, y más, basándose en los mensajes de commit.
- **Colaboración Mejorada**: Un estándar claro para los mensajes de commit mejora la comprensión entre los miembros del equipo respecto a los cambios realizados, facilitando la colaboración.
- **Mantenimiento Eficiente**: La estandarización de los mensajes de commit ayuda a mantener un historial claro y coherente, crucial para el análisis y la resolución de problemas en el proyecto.

### Consideraciones

Aunque esta configuración es universalmente aplicable a cualquier proyecto Git, requiere que Node.js y npm estén instalados en el entorno de desarrollo, lo que podría ser una consideración adicional para equipos que no utilizan Node.js en su stack tecnológico principal. Además, la adopción y el éxito de esta práctica dependen de la cultura de equipo y del acuerdo sobre la convención de mensajes de commit a seguir.

## Dependencias Requeridas

Para que el script `setup-commitlint-husky.sh` funcione correctamente y para que la validación de mensajes de commit sea efectiva, se requieren las siguientes dependencias en el entorno de desarrollo:

- **Node.js**: Un entorno de ejecución para JavaScript del lado del servidor. Es necesario para ejecutar `commitlint`, `Husky`, y otras herramientas basadas en Node.js.
- **npm** (Node Package Manager): El gestor de paquetes de Node.js, utilizado para instalar las dependencias necesarias.
- **@commitlint/cli**: La interfaz de línea de comandos para `commitlint`, que permite la validación de mensajes de commit según las reglas definidas.
- **@commitlint/config-conventional**: Un conjunto de reglas convencionales para mensajes de commit. Proporciona un punto de partida estándar para proyectos que desean seguir la convención de commits semánticos.
- **Husky**: Una herramienta que facilita la configuración de hooks de Git. Se utiliza para integrar `commitlint` en el flujo de trabajo de Git, activando la validación de mensajes de commit cada vez que se intenta hacer un commit.

## Funcionamiento de la Validación de Commit

El script configura `commitlint` con Husky para validar automáticamente los mensajes de commit contra las reglas definidas en el archivo de configuración `commitlint.config.js`. Cuando intentas hacer un commit, Husky desencadena `commitlint` para revisar el mensaje de commit según las siguientes reglas:

- **Estructura del Mensaje**: El mensaje de commit debe seguir una estructura específica, incluyendo un tipo, un ámbito opcional entre paréntesis, y una descripción. Por ejemplo: `feat(blog): add comment section`.
- **Tipos de Commit Permitidos**: Solo ciertos tipos de commit (como `feat`, `fix`, `docs`, etc.) son permitidos, lo que ayuda a mantener la coherencia y la legibilidad del historial de commits.
- **Longitud del Mensaje**: Se puede definir una longitud máxima para los mensajes de commit para evitar descripciones excesivamente largas.
- **Mayúsculas y Minúsculas**: Se pueden aplicar reglas sobre el uso de mayúsculas y minúsculas en el mensaje de commit.

Si el mensaje de commit no cumple con las reglas establecidas, `commitlint` rechaza el commit, proporcionando una retroalimentación sobre qué regla(s) no se cumplieron. Esto asegura que todos los commits en el proyecto sigan una convención coherente y mejoren la calidad del historial de cambios.

## Instalación de Dependencias

El script `setup-commitlint-husky.sh` se encarga de instalar automáticamente todas las dependencias necesarias, simplificando el proceso de configuración. Solo necesitas asegurarte de tener Node.js y npm instalados previamente en tu sistema.

## Cómo Usar el Script

1. Descarga el script `setup-commitlint-husky.sh` en la raíz de tu proyecto.
2. Otorga permisos de ejecución al script:

   ```bash
   chmod +x setup-commitlint-husky.sh
   ```

3. Ejecuta el script:

   ```bash
   ./setup-commitlint-husky.sh
   ```

## Recursos

- [Commitlint](https://commitlint.js.org/#/)
- [Husky](https://typicode.github.io/husky/#/)
- [Conventional Commits](https://www.conventionalcommits.org/)
- [Node.js](https://nodejs.org/)
- [npm](https://www.npmjs.com/)

## Contribuciones

Las contribuciones al script son bienvenidas. Si tienes sugerencias para mejorar o adaptar el script a diferentes flujos de trabajo, considera abrir un issue o un pull request en el repositorio.
