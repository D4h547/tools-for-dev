#!/bin/bash

# Comprueba si Node.js y npm están instalados
if ! command -v node &> /dev/null || ! command -v npm &> /dev/null
then
    echo "Node.js y npm son necesarios para continuar. Por favor, instálalos para proceder."
    exit 1
fi

# Inicializa npm si no existe package.json
if [ ! -f package.json ]; then
    npm init -y
fi

# Configura Husky y el hook commit-msg
mkdir -p .husky/

echo '#!/bin/sh' > .husky/commit-msg
echo 'npx --no-install commitlint --edit "$1"' >> .husky/commit-msg

# Hace el script commit-msg ejecutable
chmod +x .husky/commit-msg
npx husky install

# Crea el archivo de configuración de commitlint
echo "module.exports = {extends: ['@commitlint/config-conventional']};" >> commitlint.config.js

# Instala commitlint y Husky
npm install --save-dev @commitlint/{config-conventional,cli} husky

if command -v git &> /dev/null
then
# crear archivo commit_template.txt
FILE="commit_template.txt"
cat > $FILE <<EOL
# <tipo>(<ámbito>): <resumen corto>

# <descripción detallada>

# <información adicional>

# Tipos:
#   feat     (Nueva característica)
#   fix      (Corrección de errores)
#   docs     (Cambios en la documentación)
#   style    (Cambios de formato, falta de espacio, etc. No afecta el código)
#   refactor (Refactorización del código en producción)
#   test     (Añadiendo faltantes o corrigiendo pruebas)
#   chore    (Actualización de tareas rutinarias, etc. No cambios en el código)

# Ámbitos:
# El ámbito es opcional y puede ser cualquier sección del proyecto.
# Ejemplos: (interfaz, login, registro, carrito de compras, checkout)

# Ejemplo:
# feat(login): añadir la opción de login con redes sociales

# Notas:
# - La primera línea es la más importante. Describe brevemente qué hace el cambio y por qué.
# - Usa el modo imperativo ("añade" en lugar de "añadido" o "añadiendo").
# - La descripción detallada es opcional pero recomendada, especialmente para cambios complejos.
# - La información adicional puede incluir referencias a issues, PRs, etc.

# Elimina los comentarios antes de hacer commit.
EOL

    # agregar el template de los commits en la configuracion global de git
    git config --global commit.template commit_template.txt
fi

echo "Configuración de commitlint y Husky completada."

# agregar 
# ln -s ~/setup-commitlint-husky.sh /usr/local/bin/setup-commitlint-husky
