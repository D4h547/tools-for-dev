# <tipo>(<ámbito>): <resumen corto>

# <descripción detallada>

# <información adicional>

# Tipos:
#   feat     (Nueva característica)
#   fix      (Corrección de errores)
#   docs     (Cambios en la documentación)
#   style    (Cambios de formato, falta de espacio, etc. No afecta el código)
#   refactor (Refactorización del código en producción)
#   test     (Añadiendo faltantes o corrigiendo pruebas)
#   chore    (Actualización de tareas rutinarias, etc. No cambios en el código)

# Ámbitos:
# El ámbito es opcional y puede ser cualquier sección del proyecto.
# Ejemplos: (interfaz, login, registro, carrito de compras, checkout)

# Ejemplo:
# feat(login): añadir la opción de login con redes sociales

# Notas:
# - La primera línea es la más importante. Describe brevemente qué hace el cambio y por qué.
# - Usa el modo imperativo ("añade" en lugar de "añadido" o "añadiendo").
# - La descripción detallada es opcional pero recomendada, especialmente para cambios complejos.
# - La información adicional puede incluir referencias a issues, PRs, etc.

# Elimina los comentarios antes de hacer commit.
